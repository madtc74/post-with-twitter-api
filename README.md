## Name
Connect and Post with the Twitter API

## Description
Today we are going to use the twitter API to post a tweet to twitter for free. We are not going to be using one of the pay tier plans. This is just a simple way to connect to twitter using Jupyter Notebooks in Python.

## Youtube
https://youtu.be/HzmNUTg8xWU --> how to post msg to Twitter/X

https://youtu.be/DdPPSUUhkfk --> how to post photo to Twitter/X

https://youtu.be/njMMhJz3X4g --> how to hide your passwords in Jupyter

## Usage
Post to Twitter using Python

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing


## Authors and acknowledgment
MadTc


